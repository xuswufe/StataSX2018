
*======================================
*======================================
*            Stata 公开课  
*             peixun.net
*======================================
*======================================
*                                                       
*             主讲人：连玉君 副教授                    
*                                                       
*        单  位：中山大学岭南学院金融系                
*        电  邮: arlionn@163.com                        
*        主  页：http://dwz.cn/lianyj                   
*        博  客: http://blog.cnfol.com/arlion           
*        微  博：http://weibo.com/arlionn               
*        课  程：http://www.peixun.net/author/3.html    
*        微  信：lianyj45   公众号：Stata连享会        


*         Part 1: Stata 简介

*     --------------------------
*       B4: 帮助文件和外部命令   
*     --------------------------
  
*            ==本节目录== 
*  
*        B4.1  简介  
*        B4.2  命令的检索 
*        B4.3  管理来源于 ssc 的命令
*        B4.4  外部命令的管理和使用   
*        B4.5  更多的帮助和讨论 


  global path "`c(sysdir_personal)'\LY_stata"          
  adopath + "$path\_plus"  //外部命令统一存放于此处

  
  
*----------
*-B4.1 简介                       |peixun.net|


  help        //帮助文件目录
  help help   //help 命令的帮助文件
  help guide  //basic Stata concepts
  
  *-e.g. 
  
    help tabstat  //注意右上角的三个按钮，对于初学者很实用

	
	
*----------------
*-B4.2 命令的检索                 |peixun.net|
  
  *-模糊查询(较为灵活)

    help findit //网络搜索，查找外部命令和范例
  
    *-e.g.  
  
    findit dynamic panel  
    findit median test
  
  
  *-如何实现高效的检索            |peixun.net|
  
    help searchadvice
  
    *------------------------实现高效检索的建议-----------------------
    *
    *-(1) 单词的大小写和顺序 不影响检索结果
    *
    *-(2) 介词可以省略
    *
    *-(3) 尽量不用复数形式，distribution 优于 distributions
    *
    *-(4) 不用进行时， median test 优于 medians testing 
    *
    *-(5) 逐渐缩小检索范围：distribution --> normal distribution
    *
    *-(6) 可以用如下名词限定检索范围：
    *     data                         e.g.  findit data outlier
	*     statistics  || 简写为 stat   e.g.  findit stat median
	*     graph                        e.g.  findit graph kdensity
	*     program     || 简写为 prog   e.g.  findit loop
    *
    *-------------------------------over------------------------------

	
	
*---------------------------
*-B4.3 管理来源于 ssc 的命令      |peixun.net|
	
  *-外部命令的来源
  
    help net_mnu	
	
  *-ssc: Statistical Software Components
  
	help ssc             // http://www.repec.org/
	
    ssc whatsnew
     
	 
  *-查看来源于 SSC 的外部命令列表
	ssc des b    // 列示以 -b- 开头的所有命令，可为 a-z,以及 "_"
    ssc des x         
    ssc des winsor2      
      
  *-下载安装 ssc 命令(预先知道命令的名称)              
	ssc install winsor, replace	
	
	
  
*--------------------------
*-B4.4 外部命令的管理和使用       |peixun.net|

  *-外部命令的存放地址
  
    help net
	
	net set ado "D:\stata12\ado\personal\myplus"

	global path "D:\stata12\ado\personal\LY_stata"            
    net set ado "$path\_plus"  
	

  *-查询已安装的外部命令  -ado-, -mypkg-, -which-
  
	help ado
	
	ado                   //呈现本机上安装的所有外部命令
    ado, find(winsor)     //仅呈现包含特定关键词的外部命令
    ado, find(panel unit)
	   
	   
	help mypkg   
	   
    mypkg                 //呈现本机上已安装的外部命令
    mypkg xt*             //呈现 xt 开头的所有外部命令 
	which winsor2         //列示命令的基本信息



*----------------------
*-B4.5 更多的帮助和讨论           |peixun.net|

  
  *-Stata 官网: 常见问题解答：FAQ  
    view browse "http://www.stata.com/support/statalist/faq"
	   
  *-Stata 官网: 加入STATA用户邮件列表
    view browse "http://www.stata.com/statalist/"
	   
  *-中文: 人大经济论坛【stata专版】
    view browse "http://www.pinggu.org/bbs/forum-67-1.html"
	   
  *-中文: 人大经济论坛【VIP答疑专区】
    view browse "http://www.pinggu.org/bbs/forum-114-1.html" 
	   
	
	
	