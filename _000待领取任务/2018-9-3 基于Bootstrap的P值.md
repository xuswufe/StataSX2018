
## 简介
参见 [randReg](https://github.com/worldbank/stata/tree/master/docs)，[Randomization inference vs. bootstrapping for p-values]()
- Heß, S., 2017, Randomization inference with stata: A guide and software, Stata Journal, 17 (3): 630-651. [- PDF -](https://gitee.com/arlionn/stata/raw/master/test/ritest-sj17-3.pdf)