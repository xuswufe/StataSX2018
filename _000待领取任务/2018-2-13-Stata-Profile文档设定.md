- 目的：介绍 Stata Profile.do 文档的设定

- 思路：

  - 引言：开机显示 I Love You !
  - Stata 开机后执行命令的顺序：开机后首先会自动在安装目录下寻找名称为 profile.do 的 do 文件，并执行里面的所有语句。
  - profile 文档内容的基本设定
  - Profile 文档的花样设定
  - 提供范例 profile 文档即下载地址


### 引言：开机显示 I Love You !

这里主要通过截图来实现。

### 如何实现？

介绍 profile 文档的作用，以及 Stata 的运行机制。

Stata 开机后执行命令的顺序：开机后首先会自动在安装目录下寻找名称为 profile.do 的 do 文件，并执行里面的所有语句。


### profile 文档内容的基本设定

```stata 
set update_query off //不要自动更新

/*
dis _n(30)
if "`c(sysdir_stata)'" == "D:\stata15/"|"`c(sysdir_stata)'" == "d:\stata15/"{
   local D "D"
}
else{ 
   dis in w "   请在 command 窗口中输入 stata 所在的盘符，按回车键：______" 
   dis in w "   (e.g. 若 stata15 放置于 F 盘根目录下, 即 F:\stata15, 则输入 f 或 F 即可)"
   dis in w "   Here is my stata: " _request(isD)
   local D "$isD"
}
*/
				
				
set type double           // 设定 generate 命令产生的新变量为双精度类型
set matsize 800          // 设定矩阵的维度为 2000x2000
set scrollbufsize 2000000 // 结果窗口中显示的行数上限
set more off, perma       // 关闭分页提示符

*-可选
set cformat  %4.3f  //回归结果中系数的显示格式
set pformat  %4.3f  //回归结果中 p 值的显示格式      
set sformat  %4.2f  //回归结果中 se值的显示格式   

*-有关这一部分的完整设定命令，请输入 help set 命令进行查看
sysdir set PLUS "`c(sysdir_stata)'ado\plus"    // 外部命令的存放位置
sysdir set PERSONAL "`c(sysdir_stata)'ado\personal"  // 个人文件夹位置

*-添加其它允许stata搜索的目录，存放外部命令和自编命令
adopath + "`c(sysdir_stata)'\ado\personal\_myado"
```


### Profile 文档的花样设定


#### 开机自动创建日志文件
```stata 
* log文件：自动以当前日期为名存放于 stata13\do 文件夹下
* 若 stata13\ 下没有 do 文件夹，则本程序会自动建立一个 
cap cd `c(sysdir_stata)'do
if _rc{
   mkdir `c(sysdir_stata)'do  //检测后发现无 do 文件夹，则自行建立一个
}

*-这个盗版的stata15使用了一个插件篡改电脑时间，导致如下命令无法运行，
* 也就使得我们无法以电脑时间为基础来新建 log 文件。

local fn = subinstr("`c(current_time)'",":","-",2)
local fn1 = subinstr("`c(current_date)'"," ","",3)
log    using `c(sysdir_stata)'do\log-`fn1'-`fn'.log, text replace
cmdlog using `c(sysdir_stata)'do\cmd-`fn1'-`fn'.log, replace
```

#### Stata 15 转码方法提示

```stata 
dis in w _n(5) ///
           "    ------------------------------------------------------"
dis in w   "    -------------- Stata 15 dofile 转码方法 --------------" 
dis in w   "    ------------------------------------------------------" _n
  
  dis in w _n(1) ///
           "    用 Stata15 打开 Stata14 以下的 dofile 时，屏幕会提示 " _n
  dis in w "    ....... The document is not encoded in UTF-8! ......." _n
  dis in w "    处理方法：在 Encoding: 下拉菜单中选择 「Chinese(GBK)」，点击 OK " _n
  dis in w "    注意：不要勾选「[ ] Dot not show this message again」" _n _n
  

dis in w _n _n ///
         "    ------------------------------------------------------"
dis in w "    ----- Stata 15 转码方法(一次性处理 .dta 转码问题) ----" 
dis in w "    ------------------------------------------------------" _n
dis in w "    *-说明: dofile 或 数据文件中包含中文字符时，需要转码才能正常显示"
dis in w "                      "
dis in w "    *-Step 1: 分析当前工作路径下的编码情况(可省略)                "
dis in w "      ua: unicode analyze *                                         "    
dis in w "    *-Step 2: 设定转码类型                                          "   
dis in w "      ua: unicode encoding set gb18030  // 中文编码                 "                       
dis in w "    *-Step 3: 转换文件                                              "
dis in w "      ua: unicode translate *                                       "  
```


#### 提供一些可爱的链接

```stata 
*-常逛网址
 
  dis in w _n "   "
  
  dis _n in w _col(10) _dup(45) "="
  dis    in w _col(10) _n _skip(20) "Hello World! Hello Stata!" _n
  dis    in w _col(10) _dup(45) "=" _n 
  
  dis in w  "Stata官网：" ///
      `"{browse "http://www.stata.com": [Stata.com] }"' ///
      `"{browse "http://www.stata.com/support/faqs/":   [Stata-FAQ] }"' ///
      `"{browse "https://blog.stata.com/":      [Stata-Blogs] }"' ///
      `"{browse "http://www.stata.com/links/resources.html":   [Stata-资源链接] }"' _n
	  
  dis in w  "Stata论坛：" ///
	  `"{browse "http://www.statalist.com": [Stata-list] }"'      ///
      `"{browse "https://stackoverflow.com":  [Stack-Overflow] }"' ///
      `"{browse "http://bbs.pinggu.org/": [经管之家-人大论坛] }"'  //_n
  
  dis in w  "Stata资源：" /// 
      `"{browse "http://www.jianshu.com/u/69a30474ef33": [Stata连享会-简书] }"' ///
      `"{browse "https://www.zhihu.com/people/arlionn/":    [Stata连享会-知乎] }"'  ///
	  `"{browse "https://gitee.com/arlionn":    [Stata连享会-码云] }"'
	  
  dis in w  _col(12)  /// 
      `"{browse "http://www.jianshu.com/p/f1c4b8762709": [Stata书单] }"' ///
	  `"{browse "http://www.jianshu.com/p/c723bb0dbf98":           [Stata资源汇总] }"' //_n
	  
  dis in w  "Stata课程：" ///
      `"{browse "https://stats.idre.ucla.edu/stata/": [UCLA在线课程] }"' ///
      `"{browse "http://www.princeton.edu/~otorres/Stata/":        [Princeton在线课程] }"'  _n
	  
  dis in w  "Stata现场：" ///
	  `"{browse "http://www.peixun.net/view/307.html": [Stata初级班] }"'  ///
	  `"{browse "http://www.peixun.net/view/308.html":       [Stata高级班] }"' ///
	  `"{browse "http://i.youku.com/arlion":       [Stata优酷视频] }"' 
	  
  dis in w  "学术论文：" ///
	  `"{browse "http://scholar.chongbuluo.com/":  [学术搜索] }"'  ///
	  `"{browse "http://scholar.cnki.net/":       [CNKI] }"' ///
	  `"{browse "http://xueshu.baidu.com/":       [百度学术] }"'  ///
	  `"{browse "http://www.jianshu.com/p/494e6feab565":         [Super Link] }"' _n  
```

#### 便携小程序：快速打开特定文件夹

```stata 
*-快速进入相应目录
  dis in w _n(2) "   "  
  dis _n _n _n in w "myQuick:  sj | wx | mylec | myado | mytech | mysof | sougou | uniall"
  
  local p "sj"
  cap program drop `p'
  program define `p'
    qui cd "E:\BaiduYun\百度云同步盘\计量\STATA文档\Stata_Journal"
    qui cdout 
  end
  
  local p "mysof"
  cap program drop `p'
  program define `p'
    qui cd "E:\BaiduYun\百度云同步盘\D\Software"
    qui cdout 
  end
```

#### 一键编辑搜狗短语

```stata 
  *-------------打开搜狗短语定义---------------
  *-文件地址查询: 
  *-方法1: 可以依次点击 [开始]->[搜索]，然后输入 [Phrases.ini]
  * 进而从地址栏中复制地址
  *-方法2: 用 everything 软件查询，然后右击文件名，选择[复制完整路径和文件名(F)]
  local p "sougou" 
  cap program drop `p'
  program define `p'
    *shellout C:\Documents and Settings\Administrator\Application Data\SogouPY.users\00000001\Phrases.ini
	shellout C:\Users\Administrator\AppData\LocalLow\SogouPY.users\00000001\phrases.ini
  end
```