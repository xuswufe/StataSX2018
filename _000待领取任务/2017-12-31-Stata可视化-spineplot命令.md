
- 介绍外部命令 `spineplot` 的用法

- **安装：** 
```stata
ssc install spineplot, replace
```

- **用法：**
  - `help spineplot` //查看帮助文件中的范例
  - 该命令发布于 Stata Journal 16-2，里面有非常详细的介绍； [Stata Journal --点击下载--](https://pan.baidu.com/s/1eRZBW34) 


- 文章的结构自行拟定