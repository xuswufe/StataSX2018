# StataSX2018

### 项目介绍
Stata 连享会团队工作平台。

大家好，在现场班培训正式开始之前，我们需要写一些推文来对相关的基础知识进行介绍。这些推文都是采用 Markdown 语法写的，非常适合在网页上展示。我平时使用的 [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn)，以及有道云笔记，都充分借助了 Markdown 语法的简洁和高效。

我为每一位助教设立了一个文件夹，后续完成推文过程中的相关资料都存放在这个文件夹里。项目完成中过程中的讨论在下方的留言区里面完成。

由于在连享会的工作平台里多数的项目都是公开项目，所以我鼓励大家相互串门，了解一下其他的老师和同学们的项目的进展，互相学习。

### 注册和工作流程
1. 申请注册[码云](https://gitee.com/)账号：https://gitee.com/ 。有关账号设置和码云使用说明，请查看 [-连玉君-码云使用指南-](https://gitee.com/Stata002/Hello/tree/master/%E7%A0%81%E4%BA%91%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97)
2. 请点击 [- Stata公众号团队成员邀请码 -](https://gitee.com/Stata002/StataSX2018/invite_link?invite=61f32a90b60d1a5e588318e3c6442a30b404baa240f14a9bb7993663152babc5ba8855ba9e0d87b5383038e943f171de) 加入本项目工作小组，以便获得在小组中编辑文档的权限。

### 使用指引
- 请先阅读 [Stata连享会成员手册-Wikis](https://gitee.com/Stata002/StataSX2018/wikis/Home)，了解 Markdown 和码云的使用方法等内容。
- 然后，到 [【_000待领取任务】](https://gitee.com/Stata002/StataSX2018/tree/master/_000%E5%BE%85%E9%A2%86%E5%8F%96%E4%BB%BB%E5%8A%A1) 文件夹中选择自己感兴趣的题目，自拟提纲开始写作 (如有疑问可以随时联系我讨论)。
  - 参考项目1：[Stata连享会-侯新烁](https://gitee.com/Stata002/Hou_Xinshuo)
  - 参考项目2：[Stata连享会-卢梅](https://gitee.com/Stata002/Lumei)
- 当然，你也可以自行提出选题，我们做进一步讨论。