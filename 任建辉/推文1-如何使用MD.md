> 写在前面：Markdown 是简单标记语言

## 简介
花五分钟介绍个**好东西**—— *Markdown* ！    

---

### 什么是 Markdown ？
- 一种简单的标记语言
  - 用 `#` 表示一级标题，`##` 标记二级标题；
  - 用 `- ` 标记一个条目，缩进两格 `-` 表示二级条目；
- 代码高亮和代码块
  `regress` (我高亮了) 命令很好用：
```stata
. sysuse "auto.dta", clear
. regress price weight length
. regfit 
price =  10386.54 + 4.70*weight - 97.96*length
        (4308.16) (1.12)        (39.17)
         N = 74, R2 = 0.35, adj-R2 = 0.33
```

### 插入链接和图片
我通常用 [简书](http://www.jianshu.com) 和 [码云](https://gitee.com) 编辑器做图床。     
插入图片也可很简单 `![图名(可省)](网址)`：       
![](http://www.sysu.edu.cn/2012/images/logo.jpg)

### 数学公式
爱因斯坦的「朋友圈幸福感方程」：
$$ E = mc^2 $$        
其中，$m$ 表示 PS 后的颜值，$c$ 表示装逼能力。   
仔细想想，这真是一个伟大的公式！


> 快速上手：[十分钟 Markdown 教程](http://commonmark.cn/help/) → [在线写 Markdown，实时呈现](http://commonmark.cn/dingus/?text=&smart=1)



